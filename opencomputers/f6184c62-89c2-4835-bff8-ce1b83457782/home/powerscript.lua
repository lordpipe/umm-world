local component = require("component")
local redstone = component.redstone
local sides = require("sides")
local mainPower = component.proxy("f978b680-7a48-4fea-9f47-fead3e19eaa0")
local buffer = component.proxy("1737ba7b-d47a-4f01-9603-15c76ee8a79c")



function powerControl ()
	os.execute("resolution 95 11")
	print("power monitoring started!!!\n\n")
	while true do
		calculatePower()
		redstonePowerControl()
		displayPower()
        --print("display power not working right now.. working on it!!! sorry..")
		os.sleep(1)
	end
end

function errorValidate ()
	if buffer.getEnergy() == "Unformed." or mainPower.getEnergy() == "Unformed." then
        unformed = true
        unformedMatrix = "none"
        if buffer.getEnergy() == "Unformed." then
            unformedMatrix = "the buffer is unformed!! so the top one!!!"
        elseif mainPower.getEnergy() == "Unformed." then
            unformedMatrix = "it's the main power or bottom one!!!!!!! :D"
        elseif true then
            unformedMatrix = ":( i'm probably brokenn.. for some reason i think of them is unformed but they're not?"
        end
	    if buffer.getEnergy() == "Unformed." and mainPower.getEnergy() == "Unformed." then
            unformedMatrix = "both are unformed.. :("
        end
	elseif buffer.getEnergy() ~= "Unformed." and mainPower.getEnergy() ~= "Unformed." then
		unformed = false
	end
end

function errorCheck ()
	errorValidate()
	while unformed == true do
		print("one or both of the matrices isn't formed.. :(")
		print(unformedMatrix)
		print("\n \n \n \n \n")
		redstone.setOutput(sides.back, 0)
		redstone.setOutput(sides.left, 0)
		errorValidate()
		os.sleep(1)
	end
end

function calculatePower ()
	errorCheck()
	bufferPercent = buffer.getEnergy() / buffer.getMaxEnergy() * 100
	bufferEnergy = buffer.getEnergy()
	bufferMaxEnergy = buffer.getMaxEnergy()

	mainPowerPercent = mainPower.getEnergy() / mainPower.getMaxEnergy() * 100
	mainPowerEnergy = mainPower.getEnergy()
	mainPowerMaxEnergy = mainPower.getMaxEnergy()
end

function redstonePowerControl ()
	if bufferPercent < 20 then
		redstone.setOutput(sides.back, 15)
	end
	if bufferPercent > 90 then	
		redstone.setOutput(sides.back, 0)
	end
	if mainPowerPercent < 60 then
	redstone.setOutput(sides.left, 0)
	end
	if mainPowerPercent > 70 then
	redstone.setOutput(sides.left, 15)
	end
end

function displayPower ()
	local bufferPercentDisplay = numberToStringWithLength(bufferPercent, 25)
	local bufferEnergyRF = bufferEnergy * 0.4
	local bufferMaxEnergyRF = bufferMaxEnergy * 0.4

	local mainPowerPercentDisplay = numberToStringWithLength(mainPowerPercent, 25)
	local mainPowerEnergyRF = mainPowerEnergy * 0.4
	local mainPowerMaxEnergyRF = mainPowerMaxEnergy * 0.4

	local bufferVisual = barVisuliser(bufferPercent, 83)
	local mainPowerVisual = barVisuliser(mainPowerPercent, 83)

    local energyUnit = energyUnitifier2Inputs(bufferEnergyRF, mainPowerEnergyRF, 25, "Energy")
    local maxEnergyUnit = energyUnitifier2Inputs(bufferMaxEnergyRF, mainPowerMaxEnergyRF, 25, "MaxEnergy")

	print("name        ".."            %            ".." █ "..energyUnit.." █ "..maxEnergyUnit)
	print("buffer:     "..bufferPercentDisplay.." █ "..bufferEnergyDisplay.." █ "..bufferMaxEnergyDisplay)
	print("main power: "..mainPowerPercentDisplay.." █ "..mainPowerEnergyDisplay.." █ "..mainPowerMaxEnergyDisplay)
	print("███████████████████████████████████████████████████████████████████████████████████████████████")
	print("\n")
	print("buffer:     "..bufferVisual)
	print("main power: "..mainPowerVisual)
	print("\n")
end

function barVisuliser(percentage, characterLength)
	local characters = (characterLength - 2) * (percentage / 100)
	local bar = ""
	while #bar < characters do
		bar = bar.."#"
	end
	while #bar < characterLength - 2 do
		bar = bar.."-"
	end
	bar = "["..bar.."]"
	return bar
end

function energyUnitifier2Inputs (input1, input2, length, kind)
    local output1 = 0
    local output2 = 0
    local unit = ""

    if input1 < 1024 or input2 < 1024 then
        output1 = input1
        output2 = input2
        unit = "RF"
    elseif input1 < (1024 * 1024) and input2 < (1024 * 1024) then
        output1 = input1 / 1024
        output2 = input2 / 1024
        unit = "KiRF"
    elseif input1 < (1024 * 1024 * 1024) and input2 < (1024 * 1024 * 1024) then
        output1 = input1 / (1024 * 1024)
        output2 = input2 / (1024 * 1024)
        unit = "MiRF"
    elseif input1 < (1024 * 1024 * 1024 * 1024) and input2 < (1024 * 1024 * 1024 * 1024) then
        output1 = input1 / (1024 * 1024 * 1024)
        output2 = input2 / (1024 * 1024 * 1024)
        unit = "GiRF"
    elseif input1 < (1024 * 1024 * 1024 * 1024 * 1024) and input2 < (1024 * 1024 * 1024 * 1024 * 1024) then
        output1 = input1 / (1024 * 1024 * 1024 * 1024)
        output2 = input2 / (1024 * 1024 * 1024 * 1024)
        unit = "TiRF"
    elseif input1 < (1024 * 1024 * 1024 * 1024 * 1024 * 1024) and input2 < (1024 * 1024 * 1024 * 1024 * 1024 * 1024) then
        output1 = input1 / (1024 * 1024 * 1024 * 1024 * 1024)
        output2 = input2 / (1024 * 1024 * 1024 * 1024 * 1024)
        unit = "PiRF"
    elseif input1 > (1024 * 1024 * 1024 * 1024 * 1024 * 1024 * 1024) or input2 > (1024 * 1024 * 1024 * 1024 * 1024 * 1024 * 1024) then
        output1 = input1 / (1024 * 1024 * 1024 * 1024 * 1024 * 1024)
        output2 = input2 / (1024 * 1024 * 1024 * 1024 * 1024 * 1024)
        unit = "EiRF"
    elseif true then
        if kind == "Energy" then
            bufferEnergyDisplay = "whatever happened it"
            mainPowerEnergyDisplay = "was something weird.."
            return "how.. what?"
        elseif kind == "MaxEnergy" then
            bufferMaxEnergyDisplay = "whatever happened it"
            mainPowerMaxEnergyDisplay = "was something weird.."
            return "how.. what?"
        end
    end
    return energyUnitDisplayerThingyThing(output1, output2, length, kind, unit) 
end

function energyUnitDisplayerThingyThing (input1, input2, length, kind, unit)
    if kind == "Energy" then
        bufferEnergyDisplay = numberToStringWithLength(input1, length)
        mainPowerEnergyDisplay = numberToStringWithLength(input2, length)
        return padBoth("Energy ("..unit..")" , length, " ")
    elseif kind == "MaxEnergy" then
        bufferMaxEnergyDisplay = numberToStringWithLength(input1, length)
        mainPowerMaxEnergyDisplay = numberToStringWithLength(input2, length)
        return padBoth("MaxEnergy ("..unit..")" , length, " ")
    end
end

function numberToStringWithLength (number, length)
	local thing = string.format("%.17f", number)
	if #thing == length then
		return thing
	elseif #thing > length then
		return shorten(thing, length)
	elseif #thing < length then
		return padleft(thing, length, " ")
	end
end

function shorten(thing, length)
	return string.sub(thing, 1, length)
end

function padleft(thing, length, character)
	while #thing < length do
		thing = character..thing
	end
	return thing
end

function padBoth(thing, length, character)
    while #thing < length do
        thing = character..thing..character
    end
    return shorten(thing, length)
end

powerControl()
