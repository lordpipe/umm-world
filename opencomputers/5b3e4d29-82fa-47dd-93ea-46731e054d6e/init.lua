_OSVERSION="PsychOS 2.0a2-67de47e"

do
syslog = {}
syslog.emergency = 0
syslog.alert = 1
syslog.critical = 2
syslog.error = 3
syslog.warning = 4
syslog.notice = 5
syslog.info = 6
syslog.debug = 7

local rdprint=dprint or function() end
setmetatable(syslog,{__call = function(_,msg, level, service)
level, service = level or syslog.info, service or (os.taskInfo(os.pid()) or {}).name or "unknown"
rdprint(string.format("syslog: [%s:%d/%d] %s",service,os.pid(),level,msg))
computer.pushSignal("syslog",msg, level, service)
end})
function dprint(...)
for k,v in pairs({...}) do
syslog(v,syslog.debug)
end
end
end

do
local tTasks,nPid,nTimeout,cPid = {},1,0.25,0 -- table of tasks, next process ID, event timeout, current PID
function os.spawn(f,n) -- creates a process from function *f* with name *n*
tTasks[nPid] = {
c=coroutine.create(f), -- actual coroutine
n=n, -- process name
p=nPid, -- process PID
P=cPid, -- parent PID
e={} -- environment variables
}
if tTasks[cPid] then
for k,v in pairs(tTasks[cPid].e) do
tTasks[nPid].e[k] = tTasks[nPid].e[k] or v
end
end
nPid = nPid + 1
return nPid - 1
end
function os.kill(pid) -- removes process *pid* from the task list
tTasks[pid] = nil
end
function os.pid() -- returns the current process' PID
return cPid
end
function os.tasks() -- returns a table of process IDs
local rt = {}
for k,v in pairs(tTasks) do
rt[#rt+1] = k
end
return rt
end
function os.taskInfo(pid) -- returns info on process *pid* as a table with name and parent values
pid = pid or os.pid()
if not tTasks[pid] then return false end
return {name=tTasks[pid].n,parent=tTasks[pid].P}
end
function os.sched() -- the actual scheduler function
os.sched = nil
while #tTasks > 0 do
local tEv = {computer.pullSignal(nTimeout)}
for k,v in pairs(tTasks) do
if coroutine.status(v.c) ~= "dead" then
cPid = k
coroutine.resume(v.c,table.unpack(tEv))
else
tTasks[k] = nil
end
end
end
end
function os.setenv(k,v) -- set's the current process' environment variable *k* to *v*, which is passed to children
if tTasks[cPid] then
tTasks[cPid].e[k] = v
end
end
function os.getenv(k) -- gets a process' *k* environment variable
if tTasks[cPid] then
return tTasks[cPid].e[k]
end
end
end

-- shamelessly stolen from plan9k

buffer = {}

function buffer.new(mode, stream) -- create a new buffer in mode *mode* backed by stream object *stream*
local result = {
mode = {},
stream = stream,
bufferRead = "",
bufferWrite = "",
bufferSize = math.max(512, math.min(8 * 1024, computer.freeMemory() / 8)),
bufferMode = "full",
readTimeout = math.huge
}
mode = mode or "r"
for i = 1, unicode.len(mode) do
result.mode[unicode.sub(mode, i, i)] = true
end
local metatable = {
__index = buffer,
__metatable = "file"
}
return setmetatable(result, metatable)
end

local function badFileDescriptor()
return nil, "bad file descriptor"
end

function buffer:close()
if self.mode.w or self.mode.a then
self:flush()
end
self.closed = true
return self.stream:close()
end

function buffer:flush()
local result, reason = self.stream:write(self.bufferWrite)
if result then
self.bufferWrite = ""
else
if reason then
return nil, reason
else
return badFileDescriptor()
end
end

return self
end

function buffer:lines(...)
local args = table.pack(...)
return function()
local result = table.pack(self:read(table.unpack(args, 1, args.n)))
if not result[1] and result[2] then
error(result[2])
end
return table.unpack(result, 1, result.n)
end
end

function buffer:read(...)
local timeout = computer.uptime() + self.readTimeout

local function readChunk()
if computer.uptime() > timeout then
error("timeout")
end
local result, reason = self.stream:read(self.bufferSize)
if result then
self.bufferRead = self.bufferRead .. result
return self
else -- error or eof
return nil, reason
end
end

local function readBytesOrChars(n)
n = math.max(n, 0)
local len, sub
if self.mode.b then
len = rawlen
sub = string.sub
else
len = unicode.len
sub = unicode.sub
end
local buffer = ""
repeat
if len(self.bufferRead) == 0 then
local result, reason = readChunk()
if not result then
if reason then
return nil, reason
else -- eof
return #buffer > 0 and buffer or nil
end
end
end
local left = n - len(buffer)
buffer = buffer .. sub(self.bufferRead, 1, left)
self.bufferRead = sub(self.bufferRead, left + 1)
until len(buffer) == n

return buffer
end

local function readNumber()
local len, sub
if self.mode.b then
len = rawlen
sub = string.sub
else
len = unicode.len
sub = unicode.sub
end
local buffer = ""
local first = true
local decimal = false
local last = false
local hex = false
local pat = "^[0-9]+"
local minbuf = 3 -- "+0x" (sign + hexadecimal tag)
-- this function is used to read trailing numbers (1e2, 0x1p2, etc)
local function readnum(checksign)
local _buffer = ""
local sign = ""
while true do
if len(self.bufferRead) == 0 then
local result, reason = readChunk()
if not result then
if reason then
return nil, reason
else -- eof
return #_buffer > 0 and (sign .. _buffer) or nil
end
end
end
if checksign then
local _sign = sub(self.bufferRead, 1, 1)
if _sign == "+" or _sign == "-" then
-- "eat" the sign (Rio Lua behaviour)
sign = sub(self.bufferRead, 1, 1)
self.bufferRead = sub(self.bufferRead, 2)
end
checksign = false
else
local x,y = string.find(self.bufferRead, pat)
if not x then
break
else
_buffer = _buffer .. sub(self.bufferRead, 1, y)
self.bufferRead = sub(self.bufferRead, y + 1)
end
end
end
return #_buffer > 0 and (sign .. _buffer) or nil
end
while true do
if len(self.bufferRead) == 0 or len(self.bufferRead) < minbuf then
local result, reason = readChunk()
if not result then
if reason then
return nil, reason
else -- eof
return #buffer > 0 and tonumber(buffer) or nil
end
end
end
-- these ifs are here so we run the buffer check above
if first then
local sign = sub(self.bufferRead, 1, 1)
if sign == "+" or sign == "-" then
-- "eat" the sign (Rio Lua behaviour)
buffer = buffer .. sub(self.bufferRead, 1, 1)
self.bufferRead = sub(self.bufferRead, 2)
end
local hextag = sub(self.bufferRead, 1, 2)
if hextag == "0x" or hextag == "0X" then
pat = "^[0-9A-Fa-f]+"
-- "eat" the 0x, see https://gist.github.com/SoniEx2/570a363d81b743353151
buffer = buffer .. sub(self.bufferRead, 1, 2)
self.bufferRead = sub(self.bufferRead, 3)
hex = true
end
minbuf = 0
first = false
elseif decimal then
local sep = sub(self.bufferRead, 1, 1)
if sep == "." then
buffer = buffer .. sep
self.bufferRead = sub(self.bufferRead, 2)
local temp = readnum(false) -- no sign
if temp then
buffer = buffer .. temp
end
end
if not tonumber(buffer) then break end
decimal = false
last = true
minbuf = 1
elseif last then
local tag = sub(self.bufferRead, 1, 1)
if hex and (tag == "p" or tag == "P") then
local temp = sub(self.bufferRead, 1, 1)
self.bufferRead = sub(self.bufferRead, 2)
local temp2 = readnum(true) -- this eats the next sign if any
if temp2 then
buffer = buffer .. temp .. temp2
end
elseif tag == "e" or tag == "E" then
local temp = sub(self.bufferRead, 1, 1)
self.bufferRead = sub(self.bufferRead, 2)
local temp2 = readnum(true) -- this eats the next sign if any
if temp2 then
buffer = buffer .. temp .. temp2
end
end
break
else
local x,y = string.find(self.bufferRead, pat)
if not x then
minbuf = 1
decimal = true
else
buffer = buffer .. sub(self.bufferRead, 1, y)
self.bufferRead = sub(self.bufferRead, y + 1)
end
end
end
return tonumber(buffer)
end

local function readLine(chop)
local start = 1
while true do
local l = self.bufferRead:find("\n", start, true)
if l then
local result = self.bufferRead:sub(1, l + (chop and -1 or 0))
self.bufferRead = self.bufferRead:sub(l + 1)
return result
else
start = #self.bufferRead
local result, reason = readChunk()
if not result then
if reason then
return nil, reason
else -- eof
local result = #self.bufferRead > 0 and self.bufferRead or nil
self.bufferRead = ""
return result
end
end
end
end
end

local function readAll()
repeat
local result, reason = readChunk()
if not result and reason then
return nil, reason
end
until not result -- eof
local result = self.bufferRead
self.bufferRead = ""
return result
end

local function read(n, format)
if type(format) == "number" then
return readBytesOrChars(format)
else
if type(format) ~= "string" or unicode.sub(format, 1, 1) ~= "*" then
error("bad argument #" .. n .. " (invalid option)")
end
format = unicode.sub(format, 2, 2)
if format == "n" then
return readNumber()
elseif format == "l" then
return readLine(true)
elseif format == "L" then
return readLine(false)
elseif format == "a" then
return readAll()
else
error("bad argument #" .. n .. " (invalid format)")
end
end
end

if self.mode.w or self.mode.a then
self:flush()
end

local results = {}
local formats = table.pack(...)
if formats.n == 0 then
return readLine(true)
end
for i = 1, formats.n do
local result, reason = read(i, formats[i])
if result then
results[i] = result
elseif reason then
return nil, reason
end
end
return table.unpack(results, 1, formats.n)
end

function buffer:seek(whence, offset)
whence = tostring(whence or "cur")
assert(whence == "set" or whence == "cur" or whence == "end",
"bad argument #1 (set, cur or end expected, got " .. whence .. ")")
offset = offset or 0
checkArg(2, offset, "number")
assert(math.floor(offset) == offset, "bad argument #2 (not an integer)")

if self.mode.w or self.mode.a then
self:flush()
elseif whence == "cur" then
offset = offset - #self.bufferRead
end
local result, reason = self.stream:seek(whence, offset)
if result then
self.bufferRead = ""
return result
else
return nil, reason
end
end

function buffer:setvbuf(mode, size)
mode = mode or self.bufferMode
size = size or self.bufferSize

assert(mode == "no" or mode == "full" or mode == "line",
"bad argument #1 (no, full or line expected, got " .. tostring(mode) .. ")")
assert(mode == "no" or type(size) == "number",
"bad argument #2 (number expected, got " .. type(size) .. ")")

self.bufferMode = mode
self.bufferSize = size

return self.bufferMode, self.bufferSize
end

function buffer:getTimeout()
return self.readTimeout
end

function buffer:setTimeout(value)
self.readTimeout = tonumber(value)
end

function buffer:write(...)
if self.closed then
return badFileDescriptor()
end
local args = table.pack(...)
for i = 1, args.n do
if type(args[i]) == "number" then
args[i] = tostring(args[i])
end
checkArg(i, args[i], "string")
end

for i = 1, args.n do
local arg = args[i]
local result, reason

if self.bufferMode == "full" then
if self.bufferSize - #self.bufferWrite < #arg then
result, reason = self:flush()
if not result then
return nil, reason
end
end
if #arg > self.bufferSize then
result, reason = self.stream:write(arg)
else
self.bufferWrite = self.bufferWrite .. arg
result = self
end

elseif self.bufferMode == "line" then
local l
repeat
local idx = arg:find("\n", (l or 0) + 1, true)
if idx then
l = idx
end
until not idx
if l or #arg > self.bufferSize then
result, reason = self:flush()
if not result then
return nil, reason
end
end
if l then
result, reason = self.stream:write(arg:sub(1, l))
if not result then
return nil, reason
end
arg = arg:sub(l + 1)
end
if #arg > self.bufferSize then
result, reason = self.stream:write(arg)
else
self.bufferWrite = self.bufferWrite .. arg
result = self
end

else -- self.bufferMode == "no"
result, reason = self.stream:write(arg)
end

if not result then
return nil, reason
end
end

return self
end

function os.chdir(p) -- changes the current working directory of the calling process to the directory specified in *p*, returning true or false, error
if not (p:sub(1,1) == "/") then
local np = {}
for k,v in pairs(fs.segments(os.getenv("PWD").."/"..p)) do
if v == ".." then
np[#np] = nil
else
np[#np+1] = v
end
end
p = "/"..table.concat(np,"/")
end
if fs.list(p) then
os.setenv("PWD",p)
else
return false, "no such directory"
end
end

do
fs = {}
local fsmounts = {}

-- basics
function fs.segments(path) -- splits *path* on each /
local segments = {}
for segment in path:gmatch("[^/]+") do
segments[#segments+1] = segment
end
return segments
end
function fs.resolve(path) -- resolves *path* to a specific filesystem mount and path
if not path or path == "." then path = os.getenv("PWD") end
if path:sub(1,1) ~= "/" then path=(os.getenv("PWD") or "").."/"..path end
local segments, rpath, rfs= fs.segments(path)
local rc = #segments
for i = #segments, 1, -1 do
if fsmounts[table.concat(segments, "/", 1, i)] ~= nil then
return table.concat(segments, "/", 1, i), table.concat(segments, "/", i+1)
end
end
return "/", table.concat(segments,"/")
end

-- generate some simple functions
for k,v in pairs({"makeDirectory","exists","isDirectory","list","lastModified","remove","size","spaceUsed","spaceTotal","isReadOnly","getLabel"}) do
fs[v] = function(path)
local fsi,path = fs.resolve(path)
return fsmounts[fsi][v](path)
end
end

local function fread(self,length)
return fsmounts[self.fs].read(self.fid,length)
end
local function fwrite(self,data)
return fsmounts[self.fs].write(self.fid,data)
end
local function fseek(self,dist)
return fsmounts[self.fs].seek(self.fid,dist)
end
local function fclose(self)
return fsmounts[self.fs].close(self.fid)
end

function fs.open(path,mode) -- opens file *path* with mode *mode*
mode = mode or "rb"
local fsi,path = fs.resolve(path)
if not fsmounts[fsi] then return false end
local fid = fsmounts[fsi].open(path,mode)
if fid then
local fobj = {["fs"]=fsi,["fid"]=fid,["seek"]=fseek,["close"]=fclose}
if mode:find("r") then
fobj.read = fread
end
if mode:find("w") then
fobj.write = fwrite
end
return fobj
end
return false
end

function fs.copy(from,to) -- copies a file from *from* to *to*
local of = fs.open(from,"rb")
local df = fs.open(to,"wb")
if not of or not df then
return false
end
df:write(of:read("*a"))
df:close()
of:close()
end

function fs.rename(from,to) -- moves file *from* to *to*
local ofsi, opath = fs.resolve(from)
local dfsi, dpath = fs.resolve(to)
if ofsi == dfsi then
fsmounts[ofsi].rename(opath,dpath)
return true
end
fs.copy(from,to)
fs.remove(from)
return true
end

function fs.mount(path,proxy) -- mounts the filesystem *proxy* to the mount point *path* if it is a directory. BYO proxy.
if fs.isDirectory(path) and not fsmounts[table.concat(fs.segments(path),"/")] then
fsmounts[table.concat(fs.segments(path),"/")] = proxy
return true
end
return false, "path is not a directory"
end
function fs.umount(path)
local fsi,_ = fs.resolve(path)
fsmounts[fsi] = nil
end

function fs.mounts() -- returns a table containing the mount points of all mounted filesystems
local rt = {}
for k,v in pairs(fsmounts) do
rt[#rt+1] = k,v.address or "unknown"
end
return rt
end

function fs.address(path) -- returns the address of the filesystem at a given path, if applicable; do not expect a sensical response
local fsi,_ = fs.resolve(path)
return fsmounts[fsi].address
end
function fs.type(path) -- returns the component type of the filesystem at a given path, if applicable
local fsi,_ = fs.resolve(path)
return fsmounts[fsi].type or "filesystem"
end

fsmounts["/"] = component.proxy(computer.tmpAddress())
fs.makeDirectory("temp")
if computer.getBootAddress then
fs.makeDirectory("boot")
fs.mount("boot",component.proxy(computer.getBootAddress()))
end

end

io = {}

function io.open(path,mode) -- Open file *path* in *mode*. Returns a buffer object.
local f,e = fs.open(path, mode)
if not f then return false, e end
return buffer.new(mode,f)
end

function io.input(fd) -- Sets the default input stream to *fd* if provided, either as a buffer as a path. Returns the default input stream.
if type(fd) == "string" then
fd=io.open(fd,"rb")
end
if fd then
os.setenv("STDIN",fd)
end
return os.getenv("STDIN")
end
function io.output(fd) -- Sets the default output stream to *fd* if provided, either as a buffer as a path. Returns the default output stream.
if type(fd) == "string" then
fd=io.open(fd,"wb")
end
if fd then
os.setenv("STDOUT",fd)
end
return os.getenv("STDOUT")
end

function io.read(...) -- Reads from the default input stream.
return io.input():read(...)
end
function io.write(...) -- Writes its arguments to the default output stream.
io.output():write(...)
end

function print(...) -- Writes each argument to the default output stream, separated by newlines.
for k,v in ipairs({...}) do
io.write(tostring(v).."\n")
end
end

devfs = {}
devfs.files = {}
devfs.fds = {}
devfs.nextfd = 0
devfs.component = {}

local function rfalse()
return false
end
local function rzero()
return 0
end
function devfs.component.getLabel()
return "devfs"
end
devfs.component.spaceUsed, devfs.component.spaceTotal, devfs.component.isReadOnly, devfs.component.isDirectory,devfs.component.size, devfs.component.setLabel = rzero, rzero, rfalse, rfalse, rzero, rfalse

function devfs.component.exists(fname)
return devfs.files[fname] ~= nil
end

function devfs.component.list()
local t = {}
for k,v in pairs(devfs.files) do
t[#t+1] = k
end
return t
end

function devfs.component.open(fname, mode)
fname=fname:gsub("/","")
if devfs.files[fname] then
local r,w,c,s = devfs.files[fname](mode)
devfs.fds[devfs.nextfd] = {["read"]=r or rfalse,["write"]=w or rfalse,["seek"]=s or rfalse,["close"]=c or rfalse}
devfs.nextfd = devfs.nextfd + 1
return devfs.nextfd - 1
end
return false
end

function devfs.component.read(fd,count)
if devfs.fds[fd] then
return devfs.fds[fd].read(count)
end
end
function devfs.component.write(fd,data)
if devfs.fds[fd] then
return devfs.fds[fd].write(data)
end
end
function devfs.component.close(fd)
if devfs.fds[fd] then
devfs.fds[fd].close()
end
devfs.fds[fd] = nil
end
function devfs.component.seek(fd,...)
if devfs.fds[fd] then
return devfs.fds[fd].seek(...)
end
end
function devfs.component.remove(fname)
end
devfs.component.address = "devfs"
devfs.component.type = "devfs"

function devfs.register(fname,fopen) -- Register a new devfs node with the name *fname* that will run the function *fopen* when opened. This function should return a function for read, a function for write, function for close, and optionally, a function for seek, in that order.
devfs.files[fname] = fopen
end

fs.makeDirectory("/dev")
fs.mount("/dev",devfs.component)


devfs.register("null",function()
return function() end, function() end, function() end
end)

devfs.register("syslog",function()
return function() end, syslog, function() end end)

function loadfile(p) -- reads file *p* and returns a function if possible
local f = io.open(p,"rb")
local c = f:read("*a")
f:close()
return load(c,p,"t")
end
function runfile(p,...) -- runs file *p* with arbitrary arguments in the current thread
return loadfile(p)(...)
end
function os.spawnfile(p,n,...) -- spawns a new process from file *p* with name *n*, with arguments following *n*.
local tA = {...}
return os.spawn(function() local res={pcall(loadfile(p), table.unpack(tA))} computer.pushSignal("process_finished", os.pid(), table.unpack(res)) dprint(table.concat(res)) end,n or p)
end
_G.package = {}
package.loaded = {computer=computer,component=component,fs=fs,buffer=buffer}
function require(f) -- searches for a library with name *f* and returns what the library returns, if possible
if not package.loaded[f] then
local lib = os.getenv("LIB") or "/boot/lib"
for d in lib:gmatch("[^\n]+") do
if fs.exists(d.."/"..f) then
package.loaded[f] = runfile(d.."/"..f)
elseif fs.exists(d.."/"..f..".lua") then
package.loaded[f] = runfile(d.."/"..f..".lua")
end
end
end
if package.loaded[f] then
return package.loaded[f]
end
error("library not found: "..f)
end

do

function vt100emu(gpu) -- takes GPU component proxy *gpu* and returns a function to write to it in a manner like an ANSI terminal
local colours = {0x0,0xFF0000,0x00FF00,0xFFFF00,0x0000FF,0xFF00FF,0x00B6FF,0xFFFFFF}
local mx, my = gpu.maxResolution()
local cx, cy = 1, 1
local pc = " "
local lc = ""
local mode = "n"
local lw = true
local sx, sy = 1,1
local cs = ""
local bg, fg = 0, 0xFFFFFF

-- setup
gpu.setResolution(mx,my)
gpu.fill(1,1,mx,my," ")

function termwrite(s)
local rs = ""
s=s:gsub("\8","\27[D")
pc = gpu.get(cx,cy)
gpu.setForeground(fg)
gpu.setBackground(bg)
gpu.set(cx,cy,pc)
for i = 1, s:len() do
local cc = s:sub(i,i)

if mode == "n" then
if cc == "\n" then -- line feed
cx, cy = 1, cy+1
elseif cc == "\r" then -- cursor home
cx = 1
elseif cc == "\27" then -- escape
mode = "e"
elseif cc == "\t" then
cx = 8*((cx+9)//8)
elseif string.byte(cc) > 31 and string.byte(cc) < 127 then -- printable, I guess
gpu.set(cx, cy, cc)
cx = cx + 1
end

elseif mode == "e" then
if cc == "[" then
mode = "v"
cs = ""
elseif cc == "D" then -- scroll down
gpu.copy(1,2,mx,my-1,0,-1)
gpu.fill(1,my,mx,1," ")
cy=cy+1
mode = "n"
elseif cc == "M" then -- scroll up
gpu.copy(1,1,mx,my-1,0,1)
gpu.fill(1,1,mx,1," ")
mode = "n"
else
mode = "n"
end

elseif mode == "v" then
mode = "n"
if cc == "s" then -- save cursor
sx, sy = cx, cy
elseif cc == "u" then -- restore cursor
cx, cy = sx, sy
elseif cc == "H" then -- cursor home or to
local tx, ty = cs:match("(%d+);(%d+)")
tx, ty = tx or "1", ty or "1"
cx, cy = tonumber(tx), tonumber(ty)
elseif cc == "A" then -- cursor up
cy = cy - (tonumber(cs) or 1)
elseif cc == "B" then -- cursor down
cy = cy + (tonumber(cs) or 1)
elseif cc == "C" then -- cursor right
cx = cx + (tonumber(cs) or 1)
elseif cc == "D" then -- cursor left
cx = cx - (tonumber(cs) or 1)
elseif cc == "h" and lc == "7" then -- enable line wrap
lw = true
elseif cc == "l" and lc == "7" then -- disable line wrap
lw = false
elseif cc == "c" then
rs = string.format("%s\27[%d;%d0c",rs,mx,my)
elseif cc == "n" and lc == "6" then
rs = string.format("%s\27[%d;%dR",rs,cx,cy)
elseif cc == "m" then
for num in cs:gmatch("%d+") do
num=tonumber(num)
if num == 0 then
fg,bg = 0xFFFFFF,0
elseif num == 7 then
local nfg,nbg = bg, fg
fg, bg = nfg, nbg
elseif num > 29 and num < 38 then
fg = colours[num-29]
elseif num > 39 and num < 48 then
bg = colours[num-39]
end
end
gpu.setForeground(fg)
gpu.setBackground(bg)
else
cs = cs .. cc
if cc:match("[%d;]") then
mode = "v"
end
end
end

if cx > mx and lw then
cx, cy = 1, cy+1
end
if cy > my then
gpu.copy(1,2,mx,my-1,0,-1)
gpu.fill(1,my,mx,1," ")
cy=my
end
if cy < 1 then cy = 1 end
if cx < 1 then cx = 1 end

lc = cc
end
pc = gpu.get(cx,cy)
gpu.setForeground(bg)
gpu.setBackground(fg)
gpu.set(cx,cy,pc)
gpu.setForeground(fg)
gpu.setBackground(bg)
return rs
end

return termwrite
end
function vtemu(gpua,scra) -- creates a process to handle the GPU and screen address combination *gpua*/*scra*. Returns read, write and "close" functions.
local gpu = component.proxy(gpua)
gpu.bind(scra)
local write = vt100emu(gpu)
local kba = {}
for k,v in ipairs(component.invoke(scra,"getKeyboards")) do
kba[v]=true
end
local buf = ""
os.spawn(function() dprint(pcall(function()
while true do
local ty,ka,ch = coroutine.yield()
if ty == "key_down" and kba[ka] then
if ch == 13 then ch = 10 end
if ch == 8 then
if buf:len() > 0 then
write("\8 \8")
buf = buf:sub(1,-2)
end
elseif ch > 0 then
write(string.char(ch))
buf=buf..string.char(ch)
end
end
end
end)) end,string.format("ttyd[%s:%s]",gpua:sub(1,8),scra:sub(1,8)))
local function bread()
while not buf:find("\n") do
coroutine.yield()
end
local n = buf:find("\n")
r, buf = buf:sub(1,n), buf:sub(n+1)
return r
end
return bread, function(d) buf=buf..write(d) end, function() io.write("\27[2J\27[H") end
end
end

os.spawn(function()
os.setenv("PWD","/boot")
io.input("/dev/null")
io.output("/dev/syslog")
local f = io.open("/boot/cfg/hostname","rb")
local hostname = computer.address():sub(1,8)
if f then
hostname = f:read("*l")
f:close()
end
os.setenv("HOSTNAME",hostname)
syslog(string.format("Hostname set to %s",hostname))
local pids = {}
local rc = require "rc"
for k,v in pairs(rc.cfg.enabled) do
pids[v] = -1
dprint(v)
end
while true do
for k,v in pairs(pids) do
if not os.taskInfo(v) then
syslog("Starting service "..k)
pids[k] = rc.start(k)
end
end
coroutine.yield()
end
end, "init")

os.sched()
