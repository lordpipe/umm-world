local component = require "component"
local fs = require "fs"
local shell = require "shell"
local shutil = {}

local function wrapUnits(n)
 local scale = {"K","M","G","T","P"}
 local count = 0
 while n >= 1024 do
  count = count + 1
  if not scale[count] then return "inf" end
  n = n / 1024
 end
 return tostring(math.floor(n))..(scale[count] or "")
end

function shutil.import(lib)
 local cE = os.getenv("INCLUDE") or shell.include
 local nE = {}
 for k,v in pairs(cE) do
  nE[#nE+1] = v
 end
 require(lib)
 nE[#nE+1] = lib
 os.setenv("INCLUDE",nE)
 return true
end

function shutil.unimport(lib)
 local cE = os.getenv("INCLUDE") or shell.include
 local nE = {}
 for k,v in pairs(cE) do
  if v ~= lib then
   nE[#nE+1] = v
  end
 end
 os.setenv("INCLUDE",nE)
 return true
end

function shutil.ls(...)
 local tA = {...}
 if not tA[1] then tA[1] = "." end
 for _,d in ipairs(tA) do
  if #tA > 1 then
   print(d..":")
  end
  for _,f in ipairs(fs.list(d)) do
   print(" "..f)
  end
 end
end

function shutil.cat(...)
 for _,fn in ipairs({...}) do
  local f = io.open(fn,"rb")
  io.write(f:read("*a"))
  f:close()
 end
end

function shutil.ps()
 print("PID# Parent | Name")
 for k,v in pairs(os.tasks()) do
  local t = os.taskInfo(v)
  print(string.format("%4d   %4d | %s",v,t.parent,t.name))
 end
end

function shutil.df()
 local mt = fs.mounts()
 local ml = 0
 for k,v in pairs(mt) do
  if v:len() > ml then
   ml = v:len()
  end
 end
 local fstr = "%-"..tostring(ml).."s %5s %5s"
 print("fs"..(" "):rep(ml-2).."  size  used")
 for k,v in pairs(mt) do
  local st, su = fs.spaceTotal(v), fs.spaceUsed(v)
  print(string.format(fstr,v,wrapUnits(st),wrapUnits(su)))
 end
end

function shutil.mount(addr,path)
 if not addr then
  local mt = fs.mounts()
  for k,v in pairs(mt) do
   v = "/"..table.concat(fs.segments(v),"/")
   print(tostring(fs.address(v)).." on "..tostring(v).." type "..fs.type(v))
  end
 else
  local fscomp = component.list("filesystem")
  if not fscomp[addr] then
   for k,v in pairs(fscomp) do
    if k:find(addr) then
     print(tostring(addr).." not found, assuming you meant "..k)
     addr = k
     break
    end
   end
  end
  local proxy = component.proxy(addr)
  if not proxy then
   return false, "no such filesystem component"
  end
  print(fs.mount(path,proxy))
 end
end

function shutil.free()
 print("Total  Used  Free")
 print(string.format("%5s %5s %5s",wrapUnits(computer.totalMemory()),wrapUnits(computer.totalMemory()-computer.freeMemory()),wrapUnits(computer.freeMemory())))
end

shutil.cd = os.chdir
shutil.mkdir = fs.makeDirectory
shutil.cp = fs.copy
shutil.rm = fs.remove

return shutil
