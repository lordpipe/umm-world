local serial = require "serialization"
local shell = {}
shell.include = {"shutil"}
local function shindex(self,k)
 if rawget(self,k) then return rawget(self,k) end
 for _,v in pairs(os.getenv("INCLUDE") or shell.include) do
  if require(v)[k] then return require(v)[k] end
 end
 if package.loaded[k] then return package.loaded[k] end
 return _G[k]
end

local function formatValue(v)
 if type(v) == "table" then
  local w, rs = pcall(serial.serialize,v,true)
  if w then return rs end
 end
 return tostring(v)
end

function shell.interactive()
 local shenv = setmetatable({}, {__index=shindex})
 local run = true
 while run do
  io.write(string.format("\27[32m%s:%s>\27[0m ",os.getenv("HOSTNAME") or "localhost",(os.getenv("PWD") or _VERSION)))
  local input = io.read()
  if input:sub(1,1) == "=" then
   input = "return "..input:sub(2)
  end
  local f, r = load(input, "shell", "t", shenv)
  if not f then
   print("\27[31m"..r)
  else
   local rt = {pcall(f)}
   local rs = table.remove(rt,1)
   if not rs then io.write("\27[31m") end
   for k,v in pairs(rt) do
    print(formatValue(v))
   end
  end
 end
end

return shell
