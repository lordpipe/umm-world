local computer = require "computer"
local minitel = require "minitel"
local event = require "event"
local ufs = require "unionfs"
local rpc = require "rpc"
local netutil = {}

function netutil.importfs(host,rpath,lpath) -- import filesystem *rpath* from *host* and attach it to *lpath*
 local px = rpc.proxy(host,rpath.."_")
 function px.getLabel()
  return host..":"..rpath
 end
 px.address = host..":"..rpath
 return fs.mount(lpath,px)
end

function netutil.exportfs(path) -- export the directory *path* over RPC
 local path = "/"..table.concat(fs.segments(path),"/")
 local px = ufs.create(path)
 for k,v in pairs(px) do
  rpc.register(path.."_"..k,v)
  print(path.."_"..k)
 end
 return true
end

function netutil.ping(addr,times,timeout, silent) -- Request acknowledgment from *addr*, waiting *timeout* seconds each try, and try *times* times. If *silent* is true, don't print status. Returns true if there was at least one successful ping, the number of successes, the number of failures, and the average round trip time.
 local times, timeout = times or 5, timeout or 30
 local success, fail, time, avg = 0, 0, 0, 0
 for i = 1, times do
  local ipt = computer.uptime()
  local pid = minitel.genPacketID()
  computer.pushSignal("net_send",1,addr,0,"ping",pid)
  local t,a = event.pull(timeout,"net_ack")
  if t == "net_ack" and a == pid then
   if not silent then print("Ping reply: "..tostring(computer.uptime()-ipt).." seconds.") end
   success = success + 1
   time = time + computer.uptime()-ipt
   avg = time / success
  else
   if not silent then print("Timed out.") end
   fail = fail + 1
  end
 end
 if not silent then print(string.format("%d packets transmitted, %d received, %0.0f%% packet loss, %0.1fs",times,success,fail/times*100,time)) end
 return success > 0, success, fail, avg
end

return netutil
